#include <Arduino.h>
#include "NO_BLOCK_BLINK.h"


const int LED_PIN = 2;
NO_BLOCK_BLINK blinker = NO_BLOCK_BLINK();


void setup() {
    pinMode(LED_PIN, OUTPUT);
}


void loop() {
    // note: <NO_BLOCK_BLINK>.blink() must be called from the loop() function.
    // valid syntaxes:
    //
    // blinker.blink();                       // blink 3x times at 400ms, 1 cycle, GPIO 2
    blinker.blink(LED_PIN);                   // blink 3x times at 400ms, 1 cycle
    // blinker.blink(LED_PIN, 8);             // blink 8x at 400ms, 1 cycle
    // blinker.blink(LED_PIN, 4, 750);        // blink 4x at 750ms, 1 cycle
    // blinker.blink(LED_PIN, 4, 250, 6000);  // blink 4x at 250ms, every 6 seconds
}
