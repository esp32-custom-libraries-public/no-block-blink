#ifndef NO_BLOCK_BLINK_h
    #define NO_BLOCK_BLINK_h


    #if ARDUINO >= 100
        #include "Arduino.h"
    #else
        #include "WProgram.h"
        #include "pins_arduino.h"
        #include "WConstants.h"
    #endif


    class NO_BLOCK_BLINK {

        public:
            // constructor
            NO_BLOCK_BLINK();

            // public method
            void blink(
                int led_pin = 2,        // default
                int count = 3,          // default
                int blink_delay = 400,  // default
                int main_delay = 0      // default
            );

        private:
            // private vars
            int _blink_count = 0;
            int _led_state = 0;
            long _prev_time_blink = 0;
            long _prev_time_main = 0;
    };


#endif
