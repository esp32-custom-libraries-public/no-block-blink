#include "NO_BLOCK_BLINK.h"

NO_BLOCK_BLINK::NO_BLOCK_BLINK() {} // constructor

void NO_BLOCK_BLINK::blink(
    int led_pin,
    int count,
    int blink_delay,
    int main_delay
) {
    unsigned long curr_time_main = millis();

    if (main_delay > 0) {
        if (curr_time_main > (_prev_time_main + main_delay)) {
            _prev_time_main = curr_time_main;
            _blink_count = 0;
        }
    }

    if (_blink_count < count) {
        if (curr_time_main > (_prev_time_blink + blink_delay)) {
            _prev_time_blink = curr_time_main;
            if (_led_state == 0) {
                _led_state = 1;
            } else {
                _led_state = 0;
                _blink_count++;
            }
            digitalWrite(led_pin, _led_state);
        }
    }
}
